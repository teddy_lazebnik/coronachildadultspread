function answer = specialCases()
  global sc_initial;
  global sa_initial;
  global ic_initial;
  global ia_initial;
  global rc_initial;
  global ra_initial;
  global days;
  global needGraph;
  global alpha;
  global gama_c; 
  global gama_a;
  global beta_cc;
  global beta_aa;
  global beta_ac;
  global beta_ca;
  global Nc;
  global Na;
  
  # want to see answers
  needGraph = 1;
  
  specialCaseNoInfect();
  
endfunction

function answer = specialCaseNoInfect()
  global sc_initial;
  global sa_initial;
  global ic_initial;
  global ia_initial;
  global beta_cc;
  global beta_aa;
  global beta_ac;
  global beta_ca;
  
  ic_initial = 0;
  ia_initial = 1;
  
  # no infection between the gropus
  beta_cc = 0;
  beta_ca = 0;
  beta_ac = 0;
  beta_aa = 0;
  
  # calc dynamics
  solveModel("no_infection");
  
  # reset params
  initParams();
  
endfunction
