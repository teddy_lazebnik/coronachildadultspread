# solve the 2-age SIR model and print a graph
function answer = findJacobianMatrix()
  global alpha;
  global gama_c; 
  global gama_a;
  global beta_cc;
  global beta_aa;
  global beta_ac;
  global beta_ca;
  global Nc;
  global Na;
  global pra;
  global prc;
  
  N = Nc + Na;
  
  Sc = 0;
  Sa = N;
  Ic = 0;
  Ia = 0;
  Rc = 0;
  Ra = 0;
  Dc = 0;
  Da = 0;
  
  
  J = [[ - alpha - (Ia*beta_ca + Ic*beta_cc)/Nc,                             0,                 -(Sc*beta_cc)/Nc,         -(Sc*beta_ca)/Nc,      0,      0, 0, 0],
  [                                  alpha, -(Ia*beta_aa + Ic*beta_ac)/Na,                 -(Sa*beta_ac)/Na,         -(Sa*beta_aa)/Na,      0,      0, 0, 0],
  [           (Ia*beta_ca + Ic*beta_cc)/Nc,                             0, (Sc*beta_cc)/Nc - gama_c - alpha,          (Sc*beta_ca)/Nc,      0,      0, 0, 0],
  [                                      0,  (Ia*beta_aa + Ic*beta_ca)/Na,          alpha + (Sa*beta_ca)/Na, (Sa*beta_aa)/Na - gama_a,      0,      0, 0, 0],
  [                                      0,                             0,                       gama_c*prc,                        0, -alpha,      0, 0, 0],
  [                                      0,                             0,                                0,               gama_a*pra,      0, -alpha, 0, 0],
  [                                      0,                             0,                                0,        -gama_c*(prc - 1),      0,      0, 0, 0],
  [                                      0,                             0,                                0,        -gama_a*(pra - 1),      0,      0, 0, 0]];   
  
  display(J);
  display(det(j)); 
 
  [v, lambda] = eigs(J);
  display(lambda);

endfunction
