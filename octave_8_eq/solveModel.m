# solve the 2-age SIR model and print a graph
function y_params = solveModel(save_name = "dynamics", return_max_infected = 0, return_full_matrix = 0)
  global sc_initial;
  global sa_initial;
  global ic_initial;
  global ia_initial;
  global rc_initial;
  global ra_initial;
  global dc_initial;
  global da_initial;
  global days;
  global needGraph;
  global Nc;
  global Na;
  global pra;
  global prc;
  global last_dynamics;
  
  # consts
  HOURS_PER_DAY = 24;
  initial_condition = [sc_initial; sa_initial; ic_initial; ia_initial; rc_initial;];
  N = sc_initial + sa_initial + ic_initial + ia_initial + rc_initial + ra_initial + da_initial + dc_initial;
  
  # solve the ODEs
  # assume each step is an hour - very important
  x = lsode("twoAgeSirModel", initial_condition, (t = linspace(0, HOURS_PER_DAY * days, HOURS_PER_DAY * days)' ));
  # add x(6) and max_i for analysis
  sum_infected = zeros(size(x)(1), 1);
  for i = 1:size(x)(1)
    if x(i, 4) > Na
      x(i, 4) = Na;
    endif
    x(i, 6) = (N - x(i, 1) - x(i, 2) - x(i, 3) - x(i, 4) - x(i, 5)) * pra;
    x(i, 7) = 0;
    x(i, 8) = N - x(i, 1) - x(i, 2) - x(i, 3) - x(i, 4) - x(i, 5) - x(i, 6) - x(i, 7);
    sum_infected(i) = x(i, 3) + x(i, 4);
  endfor
  
  # find the biggest value time
  [max_i, max_i_index] = max(sum_infected);
  
  # print y - params 
  y_params = getYparameters(x);
  y_params(1) = y_params(1) / N * 100;
  
  # normalize matrix_
  for i = 1:size(x)(1)
    for j = 1:size(x)(2)
      x(i, j) = x(i, j) / N;
    endfor
  endfor
  
  # plot figure and results to this run only if needed
  if needGraph == 1
    
    printf(strcat("The max infected is:  ", mat2str(round(max_i)), " at time: ", mat2str(max_i_index)));
    
    # colors consts
    colors = {[0.859, 0.267, 0.216], [0.922, 0.114, 0.047], [0.059, 0.616, 0.345], [0.102, 0.729, 0.49], [0.259, 0.522, 0.957], [0.451, 0.627, 0.922], [0.05, 0.05, 0.05], [0.03, 0.07, 0]};
    colors_names = {"leaf green", "green", "blood red", "red", "deep blue", "blue", "black", "black"};
    colors_letters = {"g", "g", "r", "r", "b", "b", "k", "k"};
    
    printf ("\n\nMax Infected Percent of all population  = %d%%\nFinal S time = %d hours (%d days, %d hours)\nR0 adults = %f\nR0 children = %f\n\n", 
    y_params(1), 
    y_params(2), 
    floor(y_params(2) / HOURS_PER_DAY), 
    mod(y_params(2), HOURS_PER_DAY),
    y_params(3), 
    y_params(4));
    
    # plot the results
    fig = figure();
    hold on;
    for i = 1:size(x)(2)
      if (mod(i, 2) == 0)
        plot(t, x(:,i), strcat("--", colors_letters(i)));
      else
        plot(t, x(:,i), strcat("-", colors_letters(i)));
      endif
    endfor
    hold off;
    
    # set plot info
    xlim ([0, HOURS_PER_DAY * days * 1.05]);
    ylim ([0, max(max(x)) * 1.05]);
    title ("Two Age SIR Model Dynamics");
    xlabel ("Time [hours]");
    ylabel ("Normalized population size");
    legend ("susceptible child", "susceptible adult", "infected child", "infected adult", "recovered child", "recovered adult", "dead child", "dead adult");
    
    # save figure for next runs 
    file_name = strcat("answers/dynamics/", save_name ,".jpg");
    print (fig, file_name, "-djpg");
    printf("\nSaved Dynamics Figure to: '%s'\n", file_name);
    # close figures to save memory on long runs
    close all
   endif
   
   if return_max_infected == 1
     y_params = [max_i, max_i_index];
   endif
   
   last_dynamics = x;
   
endfunction

# the 2-age SIR model definition (parms and eqs)
function xdot = twoAgeSirModel(x, t)
  # use parameters
  global alpha;
  global gama_c; 
  global gama_a;
  global beta_cc;
  global beta_aa;
  global beta_ac;
  global beta_ca;
  global Nc;
  global Na;
  global pra;
  global prc;
  
  if x(4) > Na
    x(4) = Na;
  endif
  
  # model itself
  xdot(1) = -1 * (alpha + (beta_cc * x(3) + beta_ca * x(4)) / (Nc)) * x(1);
  xdot(2) = alpha * x(1) - ((beta_aa * x(4) + beta_ac * x(3)) / (Na)) * x(2);
  xdot(3) = x(1) * (beta_cc * x(3) + beta_ca * x(4)) / (Nc) - (gama_c + alpha) * (x(3));
  xdot(4) = x(2) * (beta_ca * x(3) + beta_aa * x(4)) / (Na) - gama_a * (x(4)) + alpha * x(3);
  xdot(5) = gama_c * prc * x(3) - alpha * x(5);
endfunction

function y_params = getYparameters(x)
  pick_i_c = max(x(:,3));
  pick_i_a = max(x(:,4));
  pick_infected = round(pick_i_a + pick_i_c);
  
  s_c = x(:,1);
  min_s_c = size(s_c)(1);
  for i = 1:size(s_c)(1)
    if (floor(s_c(i)) <= 1)
      min_s_c = i;
      break;
    endif
  endfor
  
  s_a = x(:,2);
  min_s_a = size(s_a)(1);
  for i = 1:size(s_a)(1)
    if (floor(s_a(i)) <= 1)
      min_s_a = i;
      break;
    endif
  endfor
  
  final_s = min(min_s_c, min_s_a);
  y_params = [pick_infected, final_s, r_adults_zero(), r_children_zero()];
endfunction

function ra_zero = r_adults_zero()
  global mu;
  global alpha;
  global gama_c; 
  global beta_cc;
  global beta_ac;
  ra_zero = beta_cc - (beta_ac + gama_c + 2 * alpha);
endfunction

function rc_zero = r_children_zero()
  global mu;
  global gama_a; 
  global beta_aa;
  global beta_ca;
  rc_zero = beta_ca - (beta_aa + gama_a);
endfunction
