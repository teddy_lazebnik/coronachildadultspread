function answer = sensitivityAnalysis()
  global sc_initial;
  global sa_initial;
  global ic_initial;
  global ia_initial;
  global rc_initial;
  global ra_initial;
  global days;
  global needGraph;
  global alpha;
  global gama_c; 
  global gama_a;
  global beta_cc;
  global beta_aa;
  global beta_ac;
  global beta_ca;
  global Nc;
  global Na;
  
  needGraph = 0;
  
  # reset params in each start run to make sure the values are the standard ones
  initParams();
  
  lockdown();
  
  stabilityPopSize();
  
  vaccine();
  
  stabilityParameterAll(8.7811 * 10 ** (-8), 8.7811 * 10 ** (-4), 100, "alpha");
  stabilityParameterAll(0, 2, 48, "gama_c");
  stabilityParameterAll(0, 2, 48, "gama_a");
  stabilityParameterAll(0, 1, 20, "beta_cc");
  stabilityParameterAll(0, 1, 100, "beta_ac");
  stabilityParameterAll(0, 1, 100, "beta_ca");
  stabilityParameterAll(0, 1, 100, "beta_aa");
endfunction

function answer = vaccine()
  global sc_initial;
  global sa_initial;
  global ic_initial;
  global ia_initial;
  global rc_initial;
  global ra_initial;
  
  steps = 20;
  
  sc_initial = 2800;
  sa_initial = 7199;
  ic_initial = 0;
  ia_initial = 1;
  rc_initial = 0;
  ra_initial = 0;
  Nc = sc_initial + ic_initial + rc_initial;
  Na = sa_initial + ia_initial + ra_initial;
  N = Na + Nc;
  
  rc_percents_move = (sc_initial) / steps;
  ra_percents_move = (sa_initial + 1) / steps; 
  
  size = steps * steps;
  answer = zeros(size, 6);
  
  tic();
  for a = 0:steps
    for c = 0:steps
      # fix params
      sc_initial = 2800 - rc_percents_move * c;
      sa_initial = 7199 - ra_percents_move * a;
      ic_initial = 0;
      ia_initial = 1;
      rc_initial = rc_percents_move * c;
      ra_initial = ra_percents_move * a;
      
      # solve model
      y_parmas = solveModel(strcat("vaccined_a_", mat2str(a), "_c_", mat2str(c)));
      answer_row = [ra_initial / Na, rc_initial / Nc, y_parmas(1), y_parmas(2), y_parmas(3), y_parmas(4)];
      index = a * steps + c + 1;
      answer(index, :) = answer_row;
      printf("Finish %d (a), %d (c) | %f%%\n", a, c, 100 * (index-1) / size);
    endfor
  endfor
  toc();
  printf("\n\n\ Finish vaccine run n\n")
  
  
  # print graphs
  vaccinePlot(answer);
endfunction


function answer = lockdown()
  global beta_cc;
  global beta_aa;
  global beta_ac;
  global beta_ca;
  global gama_c; 
  global gama_a;
  
  steps = 10;
  
  sc_initial = 28000;
  sa_initial = 71999;
  ic_initial = 0;
  ia_initial = 1;
  rc_initial = 0;
  ra_initial = 0;
  Nc = sc_initial + ic_initial + rc_initial;
  Na = sa_initial + ia_initial + ra_initial;
  N = Na + Nc;
  
  size = steps * steps;
  answer_max_infected = zeros(size, 3);
  answer_r_zero = zeros(size, 3);
  
  original_cc = beta_cc;
  original_aa = beta_aa;
  original_ac = beta_ca;
  original_ca = beta_ac;
  
  tic();
  for a = 0:steps
    for c = 0:steps
      # fix params
      beta_cc = original_cc * c / steps;
      beta_ca = original_ca * c / steps;
      beta_aa = original_aa * a / steps;
      beta_ac = original_ac * a / steps;
      
      # solve model
      y_parmas = solveModel(strcat("lock_down_a", mat2str(a), "_c_", mat2str(c)));
      index = a * steps + c + 1;
      answer_max_infected(index, :) = [a / steps, c / steps, y_parmas(1)];
      
      ccc = beta_cc / gama_c;
      aaa = beta_aa / gama_a;
      gac = gama_a * gama_c;
      
      answer_r_zero(index, :) = [a/steps, c/steps, 0.5 * ((ccc + aaa) + sqrt((aaa + ccc) ** 2 + 4 * beta_ca * beta_ac / gac))];
      printf("Finish %d (a), %d (c) | %f%%\n", a, c, 100 * (index-1) / size);
    endfor
  endfor
  toc();
  printf("\n\n\ Finish lock down run n\n")
  # savee data in file
  save lockdown_max_infected_ode.csv answer_max_infected
  save lockdown_r_zero_ode.csv answer_r_zero
endfunction

function answer = vaccinePlot(vaccineMatrix)  
  # max infected plot
  fig1 = figure();
  scatter3 (vaccineMatrix(:, 1), vaccineMatrix(:, 2), vaccineMatrix(:, 3), 10, vaccineMatrix(:, 3), "s");
  title("Vaccine max infected");
  xlabel("Percent of adults vaccined");
  ylabel("Percent of children vaccined");
  zlabel("Max Infected amount Percent  max_{t}(I_c(t) + I_a(t)) / N");
  file_name = "answers/vaccine/vaccine_max_infected.jpg";
  print (fig1, file_name, "-djpg");
  printf("\nSaved Figure to: '%s'\n", file_name);
  
  save vaccineMaxInfected.csv vaccineMatrix(:, 1:3)
  
  # reduced suspition min time
  fig2 = figure();
  scatter3 (vaccineMatrix(:, 1), vaccineMatrix(:, 2), vaccineMatrix(:, 4), 10, vaccineMatrix(:, 4), "s");
  title("Vaccine last infected");
  xlabel("Percent of adults vaccined");
  ylabel("Percent of children vaccined");
  zlabel("Last infected member [hours]");
  file_name = "answers/vaccine/vaccine_last_infected_time.jpg";
  print (fig2, file_name, "-djpg");
  printf("\nSaved Figure to: '%s'\n", file_name);
  
  # r_0 adults 
  fig3 = figure();
  scatter3 (vaccineMatrix(:, 1), vaccineMatrix(:, 2), vaccineMatrix(:, 5), 10, vaccineMatrix(:, 5), "s");
  title("Vaccine last infected");
  xlabel("Percent of adults vaccined");
  ylabel("Percent of children vaccined");
  zlabel("R_{a0}");
  file_name = "answers/vaccine/vaccine_r_zero_adult.jpg";
  print (fig3, file_name, "-djpg");
  printf("\nSaved Figure to: '%s'\n", file_name);
  
  # r_0 children 
  fig4 = figure();
  scatter3 (vaccineMatrix(:, 1), vaccineMatrix(:, 2), vaccineMatrix(:, 6), 10, vaccineMatrix(:, 6), "s");
  title("Vaccine last infected");
  xlabel("Percent of adults vaccined");
  ylabel("Percent of children vaccined");
  zlabel("R_{c0}");
  file_name = "answers/vaccine/vaccine_r_zero_children.jpg";
  print (fig4, file_name, "-djpg");
  printf("\nSaved Figure to: '%s'\n", file_name);
endfunction

function answer = stabilityPopSize()
  global sc_initial;
  global sa_initial;
  global ic_initial;
  global ia_initial;
  global rc_initial;
  global ra_initial;
  tic();
  
  steps = 6;
  sensitivityMatrix = zeros(steps, 3);
  for step = 1:steps
    
    sc_initial = (7.2 * 10 ** step) - 1;   
    Nc = sc_initial + ic_initial + rc_initial;
    sa_initial = 2.8 * 10 ** step;
    Na = sa_initial + ia_initial + ra_initial;
    
    y_parmas = solveModel(strcat("pop_size_", mat2str(Nc + Na)), 1);
    sensitivityMatrix(step, :) = [Nc + Na, y_parmas(1), y_parmas(2)];
   endfor
   
  fig1 = figure();
  plot(sensitivityMatrix(:,1), sensitivityMatrix(:, 2), "-.b");
  title("Population size on max infected time");
  xlabel("Population Size");
  ylabel("Time of max infected population [hours]");
  file_name = strcat("answers/pop_size/pop_size_analysis_time.jpg");
  print (fig1, file_name, "-djpg");
  printf("\nSaved Figure to: '%s'\n", file_name);
   
  fig2 = figure();
  plot(sensitivityMatrix(:,1), sensitivityMatrix(:, 3), "-.b");
  title("Population size on max infected percent");
  xlabel("Population Size");
  ylabel("Max infected percent of the population");
  file_name = strcat("answers/pop_size/pop_size_analysis_max_infected.jpg");
  print (fig2, file_name, "-djpg");
  printf("\nSaved Figure to: '%s'\n", file_name);
   
  initParams();
endfunction

function answer = stabilityParameterAll(start_value, end_value, steps, param)
  tic();
  stabilityParameterPlot(stabilityParameterCalc(start_value, end_value, steps, param), param);
  toc();
  printf("\n\n")
  
  # reset parameters
  initParams();
endfunction

function sensitivity = stabilityParameterCalc(start_value, end_value, steps, param)  
  global sc_initial;
  global sa_initial;
  global ic_initial;
  global ia_initial;
  global rc_initial;
  global ra_initial;
  global days;
  global needGraph;
  global alpha;
  global gama_c; 
  global gama_a;
  global beta_cc;
  global beta_aa;
  global beta_ac;
  global beta_ca;
  global Nc;
  global Na;
  
  param_step = (end_value - start_value) / steps;
  sensitivity = zeros(steps, 5);
  for step = 1:steps
    
    printf("Ineration %d / %d for param %s\n", step, steps, param);
    
    param_value = start_value + (step - 1) * param_step;
    
    if strncmp(param, "sc_initial", length(param))
      sc_initial = param_value;   
      Nc = sc_initial + ic_initial + rc_initial;
    elseif  strncmp(param, "sa_initial", length(param))
      sa_initial = param_value;
      Na = sa_initial + ia_initial + ra_initial;
    elseif  strncmp(param, "ic_initial", length(param))
      ic_initial = param_value
      Nc = sc_initial + ic_initial + rc_initial;
    elseif  strncmp(param, "ia_initial", length(param))
      ia_initial = param_value;
      Na = sa_initial + ia_initial + ra_initial;
    elseif  strncmp(param, "rc_initial", length(param))
      rc_initial = param_value;
      Nc = sc_initial + ic_initial + rc_initial;
    elseif  strncmp(param, "ra_initial", length(param))
      ra_initial = param_value;
      Na = sa_initial + ia_initial + ra_initial;
    elseif  strncmp(param, "alpha", length(param))
      alpha = param_value;
    elseif  strncmp(param, "gama_c", length(param))
      gama_c = param_value;
    elseif  strncmp(param, "gama_a", length(param))
      gama_a = param_value;
    elseif  strncmp(param, "beta_cc", length(param))
      beta_cc = param_value;
    elseif  strncmp(param, "beta_aa", length(param))
      beta_aa = param_value;
    elseif  strncmp(param, "beta_ac", length(param))
      beta_ac = param_value;
    elseif  strncmp(param, "beta_ca", length(param))
      beta_ca = param_value;
    endif
    
    y_parmas = solveModel(strrep(strcat(param, "_", mat2str(param_value)), ".", "-"));
    sensitivity(step, :) = [param_value, y_parmas(1), y_parmas(2), y_parmas(3), y_parmas(4)];
  endfor
  
  printf("\nFinished param '%s'\n", param);
  
endfunction

function answer = stabilityParameterPlot(sensitivityMatrix, param)  
  
  paramShow = ParamShowName(param);
  
  # max infected plot
  fig1 = figure();
  plot(sensitivityMatrix(:,1), sensitivityMatrix(:, 2), "-.b");
  title(strcat(paramShow, " - stability analysis plot (max infected)"));
  xlabel(strcat(paramShow, " parameter value"));
  ylabel("Max Infected amount Percent  max_{t}(I_c(t) + I_a(t)) / N");
  file_name = strcat("answers/max_infected/", param, "_max_infected.jpg");
  print (fig1, file_name, "-djpg");
  printf("\nSaved Figure to: '%s'\n", file_name);
  
  # reduced suspition min time
  fig2 = figure();
  plot(sensitivityMatrix(:,1), sensitivityMatrix(:, 3), "-.b");
  title(strcat(paramShow, " - stability analysis plot (last infected)"));
  xlabel(strcat(paramShow, " parameter value"));
  ylabel("Last infected member [hours]");
  file_name = strcat("answers/last_infected/", param, "_last_infected_time.jpg");
  print (fig2, file_name, "-djpg");
  printf("Saved Figure to: '%s'\n", file_name);
  
  # r_0 adults + children
  fig3 = figure();
  hold on;
  plot(sensitivityMatrix(:,1), sensitivityMatrix(:, 4), "-xb");
  plot(sensitivityMatrix(:,1), sensitivityMatrix(:, 5), "-dm");
  plot(sensitivityMatrix(:,1), ones(size(sensitivityMatrix(:,1))), "-k");
  hold off;
  title(strcat(paramShow, " - stability analysis plot (R0)"));
  xlabel(strcat(paramShow, " parameter value"));
  ylabel("R_0");
  file_name = strcat("answers/r_zero/", param, "_r0.jpg");
  print (fig3, file_name, "-djpg");
  printf("Saved Figure to: '%s'\n\n", file_name);
  legend ("R_{a0}", "R_{c0}");
  
  # close figures to save memory on long runs
  close all
endfunction

function param = ParamShowName(param)
  if any(strfind(param, "_"))
    param = strcat(strrep(param, "_", "_{"), "}");
  endif
  if any(strfind(param, "gama"))
    param = strrep(param, "gama", "gamma");
  endif
  if any(strncmp({"gama", "alpha", "beta"}, param, 2))
    param = strcat("\\", param);
  endif
endfunction
