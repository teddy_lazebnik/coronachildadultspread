syms Sc Sa Ic Ia Rc Ra Dc Da alpha beta_cc beta_ca beta_ac beta_aa gama_c gama_a prc pra Nc Na

s = [Sc; Sa; Ic; Ia; Rc; Ra; Dc; Da];
f = [-1 * (alpha + (beta_cc * Ic + beta_ca * Ia) / (Nc)) * Sc, alpha * Sc - ((beta_aa * Ia + beta_ac * Ic) / (Na)) * Sa, Sc * (beta_cc * Ic + beta_ca * Ia) / (Nc) - (gama_c + alpha) * (Ic), Sa * (beta_ca * Ic + beta_aa * Ia) / (Na) - gama_a * (Ia) + alpha * Ic, gama_c * prc * Ic - alpha * Rc, gama_a * pra * Ia - alpha * Ra, gama_c * (1 - prc) * Ia, gama_a * (1 - pra) * Ia];

J = jacobian(f, s);
disp(J);

# use in the intresting points
intresting_points = [];

j = [];
for pointIndex = 1:length(intresting_points)
	j[pointIndex] = subs(J, s, intresting_points[pointIndex]);
end