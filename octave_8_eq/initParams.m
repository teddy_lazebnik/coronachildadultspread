### GLOBAL PARAMS ###
# values from the paper's table - normalize to [hour] units if units not in hours
global alpha;
global gama_c; 
global gama_a;
global beta_cc;
global beta_aa;
global beta_ac;
global beta_ca;
global Nc;
global Na;
global pra;
global prc;
global sc_initial;
global sa_initial;
global ic_initial;
global ia_initial;
global rc_initial;
global ra_initial;
global dc_initial;
global da_initial;
global days;
global needGraph;
global last_dynamics;


### END - GLOBAL PARAMS ###

function none = initParams()
  global alpha;
  global gama_c; 
  global gama_a;
  global beta_cc;
  global beta_aa;
  global beta_ac;
  global beta_ca;
  global Nc;
  global Na;
  global pra;
  global prc;
  global sc_initial;
  global sa_initial;
  global ic_initial;
  global ia_initial;
  global rc_initial;
  global ra_initial;
  global dc_initial;
  global da_initial;
  global days;
  global last_dynamics;
  
  # manully picked
  days = 84;
  
  needGraph = 1;
  
  # set initial condition
  sc_initial = 28000;
  sa_initial = 71999;
  ic_initial = 0;
  ia_initial = 1;
  rc_initial = 0;
  ra_initial = 0;
  dc_initial = 0;
  da_initial = 0;
  
  # model parms
  alpha = 8.7811 * 10 ** (-6);
  gama_c = 1 / (2 * 24); 
  gama_a = 1 / (14 * 24);
  beta_cc = 0.308;
  beta_aa = 0.308;
  beta_ac = 0;
  beta_ca = 0.266;
  pra = 0.942;
  prc = 1.0;
  
  # pop sums
  Nc = sc_initial + ic_initial + rc_initial + dc_initial;
  Na = sa_initial + ia_initial + ra_initial + da_initial;
  
  # last dynamics
  last_dynamics = [];
endfunction