# show the bifurcations in the model
function answer = bifurcations()
  global sc_initial;
  global sa_initial;
  global ic_initial;
  global ia_initial;
  global rc_initial;
  global ra_initial;
  global days;
  global needGraph;
  global alpha;
  global gama_c; 
  global gama_a;
  global beta_cc;
  global beta_aa;
  global beta_ac;
  global beta_ca;
  global Nc;
  global Na;
  
  # reset parameters
  initParams();
  initParams();
  
  # show graphs
  needGraph = 1;
  
  # beta_{cc} bifurcation
  beta_cc = 0.1;
  y_parmas = solveModel(strrep(strcat("beta_cc", "_", mat2str(beta_cc)), ".", "-"));
  bifo_beta_cc(1, :) = [beta_cc, y_parmas(1), y_parmas(2), y_parmas(3), y_parmas(4)];
  beta_cc = 0.8;
  y_parmas = solveModel(strrep(strcat("beta_cc", "_", mat2str(beta_cc)), ".", "-"));
  bifo_beta_cc(1, :) = [beta_cc, y_parmas(1), y_parmas(2), y_parmas(3), y_parmas(4)];
  
  
  
endfunction
