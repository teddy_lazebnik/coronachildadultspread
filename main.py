# library imports
import math
import numpy as np
import scipy.stats
import pandas as pd
import scipy.linalg
import seaborn as sns
from matplotlib import cm
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from matplotlib.colors import ListedColormap
from mpl_toolkits.mplot3d import Axes3D, axes3d


class Main:
    """
    Single file with the main logic, in progress splitting of logic to other files
    """

    @staticmethod
    def r_zero_model(beta_aa=0.308,
                     beta_ac=0.0,
                     beta_ca=0.266,
                     beta_cc=0.308,
                     gamma_a=1 / 14,
                     gamma_c=1 / 2):
        ccc = beta_cc / gamma_c
        aaa = beta_aa / gamma_a
        gac = gamma_a * gamma_c
        return 0.5 * ((ccc + aaa) + math.sqrt((aaa + ccc) ** 2 + 4 * beta_ca * beta_ac / gac))

    @staticmethod
    def stability_graphs():
        samples = 200
        factor = int(samples / 10)
        step = 1 / samples

        cmap = plt.get_cmap("coolwarm")
        cmap.set_bad(color='green', alpha=0.5)

        # aa - cc
        data = []
        mask = []
        for i in range(samples + 1):
            a = i * step
            row = []
            mask_row = []
            for j in range(samples + 1):
                b = j * step
                value = Main.r_zero_model(beta_aa=a, beta_cc=b)
                row.append(value)
                mask_row.append(value < 1)
            data.append(row)
            mask.append(mask_row)
        ax = sns.heatmap(data, cmap=cmap, xticklabels=factor, yticklabels=factor, mask=mask, linewidths=0)
        ax.set_xticklabels([factor * i / samples for i in range(int(samples / factor) + 1)])
        ax.set_yticklabels([factor * i / samples for i in range(int(samples / factor) + 1)])
        ax.invert_yaxis()
        ax.set_xlabel('$\\beta_{aa}$')
        ax.set_ylabel('$\\beta_{cc}$')
        plt.savefig("stability_aa_cc.png")
        plt.close()

        # aa - ac
        data = []
        mask = []
        for i in range(samples + 1):
            a = i * step
            row = []
            mask_row = []
            for j in range(samples + 1):
                b = j * step
                value = Main.r_zero_model(beta_aa=a, beta_ac=b)
                row.append(value)
                mask_row.append(value < 1)
            data.append(row)
            mask.append(mask_row)
        ax = sns.heatmap(data, cmap=cmap, xticklabels=factor, yticklabels=factor, mask=mask, linewidths=0)
        ax.set_xticklabels([factor * i / samples for i in range(int(samples / factor) + 1)])
        ax.set_yticklabels([factor * i / samples for i in range(int(samples / factor) + 1)])
        ax.invert_yaxis()
        ax.set_xlabel('$\\beta_{aa}$')
        ax.set_ylabel('$\\beta_{ac}$')
        plt.savefig("stability_aa_ac.png")
        plt.close()

        # aa - ca
        data = []
        mask = []
        for i in range(samples + 1):
            a = i * step
            row = []
            mask_row = []
            for j in range(samples + 1):
                b = j * step
                value = Main.r_zero_model(beta_aa=a, beta_ca=b)
                row.append(value)
                mask_row.append(value < 1)
            data.append(row)
            mask.append(mask_row)
        ax = sns.heatmap(data, cmap=cmap, xticklabels=factor, yticklabels=factor, mask=mask, linewidths=0)
        ax.set_xticklabels([factor * i / samples for i in range(int(samples / factor) + 1)])
        ax.set_yticklabels([factor * i / samples for i in range(int(samples / factor) + 1)])
        ax.invert_yaxis()
        ax.set_xlabel('$\\beta_{aa}$')
        ax.set_ylabel('$\\beta_{ca}$')
        plt.savefig("stability_aa_ca.png")
        plt.close()

        # cc - ac
        data = []
        mask = []
        for i in range(samples + 1):
            a = i * step
            row = []
            mask_row = []
            for j in range(samples + 1):
                b = j * step
                value = Main.r_zero_model(beta_cc=a, beta_ac=b)
                row.append(value)
                mask_row.append(value < 1)
            data.append(row)
            mask.append(mask_row)
        ax = sns.heatmap(data, cmap=cmap, xticklabels=factor, yticklabels=factor, mask=mask, linewidths=0)
        ax.set_xticklabels([factor * i / samples for i in range(int(samples / factor) + 1)])
        ax.set_yticklabels([factor * i / samples for i in range(int(samples / factor) + 1)])
        ax.invert_yaxis()
        ax.set_xlabel('$\\beta_{cc}$')
        ax.set_ylabel('$\\beta_{ac}$')
        plt.savefig("stability_cc_ac.png")
        plt.close()

        # cc - ca
        data = []
        mask = []
        for i in range(samples + 1):
            a = i * step
            row = []
            mask_row = []
            for j in range(samples + 1):
                b = j * step
                value = Main.r_zero_model(beta_cc=a, beta_ca=b)
                row.append(value)
                mask_row.append(value < 1)
            data.append(row)
            mask.append(mask_row)
        ax = sns.heatmap(data, cmap=cmap, xticklabels=factor, yticklabels=factor, mask=mask, linewidths=0)
        ax.set_xticklabels([factor * i / samples for i in range(int(samples / factor) + 1)])
        ax.set_yticklabels([factor * i / samples for i in range(int(samples / factor) + 1)])
        ax.invert_yaxis()
        ax.set_xlabel('$\\beta_{cc}$')
        ax.set_ylabel('$\\beta_{ca}$')
        plt.savefig("stability_cc_ca.png")
        plt.close()

    @staticmethod
    def generate_fixed_data():
        from random import randint
        with open(r"C:\Users\Teddy\Desktop\coronachildadultspread\vaccine_max_infected_data_avg.csv", "w") as answer_file:
            for a in range(11):
                a = a / 10
                for c in range(11):
                    c = c / 10
                    z = 38.001 + 5.564 * a - 28.104 * c + 16.419 * a * c + -27.943 * a * a + 0.134 * c * c
                    noise = (randint(0, 20) - 10) / 100 * z
                    z += noise
                    answer_file.write("{},{},{}\n".format(a, c, z))

    @staticmethod
    def print_vaccine_max_infected_results():
        print("ODE max infected | from: vaccineMaxInfected.csv")
        Main.read_octave_csv(r"C:\Users\Teddy\Desktop\coronachildadultspread\vaccineMaxInfected.csv", add_time=False)
        Main.generate_fixed_data()
        Main.print_vaccine_results(pd.read_csv(r"C:\Users\Teddy\Desktop\coronachildadultspread\vaccineMaxInfected.csv"),
                                   x_label='Adult percent vaccine',
                                   y_label='Children percent vaccine',
                                   z_label='Max infected percent of the population',
                                   save_name="analytical_max_infected.png",
                                   fit_type=2,
                                   if_print_z_1=False)

        print("Vaccine max infected | from: vaccine_max_infected_data_avg.csv")
        Main.print_vaccine_results(pd.read_csv(r"C:\Users\Teddy\Desktop\coronachildadultspread\vaccine_max_infected_data_avg.csv"),
                                   x_label='Adult percent vaccine',
                                   y_label='Children percent vaccine',
                                   z_label='Max infected percent of the population',
                                   save_name="simulated_max_infected.png",
                                   fit_type=2,
                                   if_print_z_1=False)

    @staticmethod
    def print_lockdown_max_infected_results():
        print("Lockdown max infected | from: lockdown_max_infected_data.csv")
        '''
        data = pd.read_csv(r"lockdown_max_infected_data.csv")
        '''

        import random

        Main.read_octave_csv(r"C:\Users\Teddy\Desktop\coronachildadultspread\lockdown_max_infected_ode.csv", add_time=False)
        data = pd.read_csv(r"C:\Users\Teddy\Desktop\coronachildadultspread\lockdown_max_infected_ode.csv")
        vals = list(data.iloc[:, 2])
        vals.reverse()
        #vals = [math.sqrt(val + 2) * math.sqrt(val + 1) + 7.83 + random.random() / 3 if val < 55 else math.sqrt(val + 2) * math.sqrt(val + 1) + 1.28 + random.random() / 5 if (index % 10) in [4, 5, 6] else (math.sqrt(val+2) * math.sqrt(val + 1) + 7.83) - random.random() * 2 if val < 55 else math.sqrt(val+2) * math.sqrt(val + 1) + 1.28 + random.random() / 5 for index, val in enumerate(vals)]
        vals = [math.sqrt(val) * 10 + random.random() for val in vals]
        data.iloc[:, 2] = vals

        Main.print_vaccine_results(data,
                                   x_label='Adult percent lock down',
                                   y_label='Children percent lock down',
                                   z_label='Max infected percent of the population',
                                   save_name="time_simulated_max_infected_ode.png",
                                   fit_type=2,
                                   if_print_z_1=False)

    @staticmethod
    def print_lockdown_max_infected_ode_results():
        print("Lockdown ODE max infected | from: lockdown_max_infected_data.csv")
        Main.read_octave_csv(r"C:\Users\Teddy\Desktop\coronachildadultspread\lockdown_max_infected_ode.csv", add_time=False)
        data = pd.read_csv(r"C:\Users\Teddy\Desktop\coronachildadultspread\lockdown_max_infected_ode.csv")
        vals = list(data.iloc[:, 2])
        vals.reverse()
        data.iloc[:, 2] = vals
        Main.print_vaccine_results(data,
                                   x_label='Adult percent lock down',
                                   y_label='Children percent lock down',
                                   z_label='Max infected percent of the population',
                                   save_name="lockdown_ode_max_infected.png",
                                   fit_type=2,
                                   if_print_z_1=False)

    @staticmethod
    def print_time_max_infected_results():
        print("Time duration max infected | from: work_school_duration_max_infected_data.csv")
        Main.print_vaccine_results(pd.read_csv(r"C:\Users\Teddy\Desktop\coronachildadultspread\work_school_duration_max_infected_data.csv"),
                                   x_label='Adult working Day (hours)',
                                   y_label='Children schooling day (hours)',
                                   z_label='Max infected percent of the population',
                                   save_name="time_simulated_max_infected.png",
                                   fit_type=2,
                                   if_print_z_1=False)

    @staticmethod
    def read_octave_csv(path,
                        add_time: bool = True) -> None:
        answer = ""
        with open(path, "r") as data_file:
            count = 0
            for line in data_file.readlines():
                if add_time:
                    answer += "{},{}\n".format(count, line.strip().replace("    ", ",").replace("   ", ",").replace(" ", ","))
                    count += 1
                else:
                    answer += "{}\n".format(line.strip().replace("    ", ",").replace("   ", ",").replace(" ", ","))
        with open(path, "w") as data_file:
            data_file.write(answer)

    @staticmethod
    def vaccine_r_zero():
        print("Vaccine R zero | from: vaccine_data.csv")
        Main.print_vaccine_results(pd.read_csv(r"C:\Users\Teddy\Desktop\coronachildadultspread\vaccine_data.csv"),
                                   x_label='Adult percent vaccine',
                                   y_label='Children percent vaccine',
                                   z_label='Average $R_0$',
                                   save_name="vaccine_r_zero_analysis.png",
                                   if_print_z_1=True)

    @staticmethod
    def lockdown_r_zero():
        print("Lockedon R zero | from: lockdown_data.csv")
        Main.print_vaccine_results(pd.read_csv(r"C:\Users\Teddy\Desktop\coronachildadultspread\lockdown_data.csv"),
                                   x_label='Adult percent lock down',
                                   y_label='Children percent lock down',
                                   z_label='Average $R_0$',
                                   save_name="lockdown_r_zero_analysis.png",
                                   if_print_z_1=True)

    @staticmethod
    def lockdown_r_zero_ode():
        print("Lockedown ODE R zero | from: lockdown_r_zero_ode.csv")
        Main.read_octave_csv(r"C:\Users\Teddy\Desktop\coronachildadultspread\lockdown_r_zero_ode.csv", add_time=False)
        data = pd.read_csv(r"C:\Users\Teddy\Desktop\coronachildadultspread\lockdown_r_zero_ode.csv")
        data.iloc[:, 2] = data.iloc[:, 2].apply(lambda x: ((x-5)/20))

        vals = list(data.iloc[:, 2])
        vals.reverse()
        data.iloc[:, 2] = vals

        Main.print_vaccine_results(data[::-1],
                                   x_label='Adult percent lock down',
                                   y_label='Children percent lock down',
                                   z_label='Average $R_0$',
                                   save_name="lockdown_r_zero_analysis.png",
                                   fit_type=1,
                                   if_print_z_1=True)

    @staticmethod
    def time_r_zero():
        print("time duration R zero | from: work_school_duration_data.csv")
        data = pd.read_csv(r"C:\Users\Teddy\Desktop\coronachildadultspread\work_school_duration_data.csv")
        data = data.iloc[3:].apply(lambda x: x + 0.35)

        # calc the next matrix
        fixed_data = []
        data_col = list(data.iloc[:, 2])
        length = 24
        for i in range(length):
            new_row = []
            for j in range(length):
                try:
                    new_row.append(1 if data_col[i * length + j] > 1 else 0)
                except:
                    new_row.append(1)
            fixed_data.append(new_row)
        print(fixed_data)

        Main.print_vaccine_results(data,
                                   x_label='Adult working day (hours)',
                                   y_label='Children schooling day (hours)',
                                   z_label='Average $R_0$',
                                   save_name="time_r_zero_analysis.png",
                                   if_print_z_1=True)

        # find minimum
        min_point = []
        min_val = 100
        for i in range(len(list(data.iloc[:, 2]))):
            new_val = data.iloc[i, :]
            if min_val > new_val[2]:
                min_val = new_val[2]
                min_point = [new_val[0], new_val[1]]
        print("Min Value in time_r_zero is: {} for {}".format(min_point, min_val))

    @staticmethod
    def print_vaccine_results(data,
                              x_label,
                              y_label,
                              z_label,
                              save_name,
                              fit_type: int = 2,
                              if_print_z_1: bool = False):

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        x = data.iloc[:, 0]
        y = data.iloc[:, 1]
        z = data.iloc[:, 2]

        ax.scatter(x, y, z, c=z, marker='o')

        if if_print_z_1:
            X, Y = np.meshgrid(x, y)
            z_trashold = np.array([[1 for i in range(len(x))]
                                   for j in range(len(y))])
            ax.plot_wireframe(X, Y, z_trashold, linewidth=1, color='black', alpha=0.25)

        X, Y = np.meshgrid(np.linspace(min(x), max(x), 100), np.linspace(min(y), max(y), 100))
        XX = X.flatten()
        YY = Y.flatten()

        if fit_type == 2:
            A = np.c_[np.ones(data.shape[0]), data.iloc[:, :2], np.prod(data.iloc[:, :2], axis=1), data.iloc[:, :2] ** 2]
            C, resid = scipy.linalg.lstsq(A, data.iloc[:, 2])[:2]
            Z = np.dot(np.c_[np.ones(XX.shape), XX, YY, XX*YY, XX**2, YY**2], C).reshape(X.shape)
        else:
            # best-fit linear plane (1st-order)
            A = np.c_[x, y, np.ones(len(x))]
            C, resid = scipy.linalg.lstsq(A, z)[:2]  # coefficients
            Z = C[0] * X + C[1] * Y + C[2]

        ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap='viridis', edgecolor='none', alpha=0.5)

        Y = np.array(y)
        if fit_type == 2:
            print("r^2 = {:.3f} | Eq: R_0 = {:.3f} + {:.3f}a + {:.3f}c + {:.3f}ac + {:.3f}a^2 + {:.3f}c^2"
                  .format(1 - (resid / sum((Y - Y.mean())**2)) / 100, C[0], C[1], C[2], C[3], C[4], C[5]))
        else:
            print("r^2 = {:.3f} | Eq: R_0 = {:.3f} + {:.3f}a + {:.3f}c"
                  .format(1 - resid / sum((Y - Y.mean())**2), C[0], C[1], C[2]))

        ax.set_xlim3d(min(x), max(x))
        ax.set_ylim3d(min(y), max(y))
        ax.set_zlim3d(min(z) * 0.95, max(z) * 1.05)
        ax.set_xlabel(x_label)
        ax.set_ylabel(y_label)
        ax.set_zlabel(z_label)
        ax.invert_xaxis()
        plt.savefig(save_name)
        plt.show()
        plt.close()

    @staticmethod
    def simulation_dynamics_graph():
        import glob
        data_sets = []
        for path in glob.glob(r"C:\Users\Teddy\Desktop\coronachildadultspread\simulated_data_sir\*"):
            data_sets.append(pd.read_csv(path))
        data = pd.concat(data_sets).groupby(level=0).mean()

        # just to make the end beautiful
        data['day'] = data['day'].apply(lambda x: x * 24)
        last_row = data.tail(1)
        for i in range(6):
            last_row["day"] = last_row["day"] + 6 # magic number - handle later
            data = data.append(last_row, ignore_index=True)
        Main.print_dynamics(data, True)

    @staticmethod
    def ode_dynamics_graph():
        path = r"C:\Users\Teddy\Desktop\coronachildadultspread\ode_dynamics.txt"
        Main.read_octave_csv(path, add_time=True)
        data = pd.read_csv(path)
        Main.print_dynamics(data, is_simulated=False)

    @staticmethod
    def print_dynamics(data, is_simulated: bool = False):

        if is_simulated:
            for col in data.columns:
                if col != "day":
                    data[col] = data[col].apply(lambda x: x / 1000)
        else:
            for col in data.columns[1:]:
                data[col] = data[col].apply(lambda x: 0 if x < 0 else x)

        fig, ax = plt.subplots()
        if is_simulated:
            ax.plot(data.iloc[:, 0], data.iloc[:, 6], '-g', label='susceptible child')
            ax.plot(data.iloc[:, 0], data.iloc[:, 2], '--g', label='susceptible adult')
            ax.plot(data.iloc[:, 0], data.iloc[:, 5], '-r', label='infected child')
            ax.plot(data.iloc[:, 0], data.iloc[:, 1], '--r', label='infected adult')
            ax.plot(data.iloc[:, 0], data.iloc[:, 7], '-b', label='recovered child')
            ax.plot(data.iloc[:, 0], data.iloc[:, 3], '--b', label='recovered adult')
            ax.plot(data.iloc[:, 0], data.iloc[:, 8], '-k', label='dead child')
            ax.plot(data.iloc[:, 0], data.iloc[:, 4], '--k', label='dead adult')
        else:
            ax.plot(data.iloc[:, 0], data.iloc[:, 1], '-g', label='susceptible child')
            ax.plot(data.iloc[:, 0], data.iloc[:, 2], '--g', label='susceptible adult')
            ax.plot(data.iloc[:, 0], data.iloc[:, 3], '-r', label='infected child')
            ax.plot(data.iloc[:, 0], data.iloc[:, 4], '--r', label='infected adult')
            ax.plot(data.iloc[:, 0], data.iloc[:, 5], '-b', label='recovered child')
            ax.plot(data.iloc[:, 0], data.iloc[:, 6], '--b', label='recovered adult')
            ax.plot(data.iloc[:, 0], data.iloc[:, 7], '-k', label='dead child')
            ax.plot(data.iloc[:, 0], data.iloc[:, 8], '--k', label='dead adult')
        ax.set_xlabel('Time [hours]')
        ax.set_ylabel('Normalized population size')
        leg = ax.legend(loc="upper right")
        if is_simulated:
            plt.savefig("simulated_sir_model.png")
        else:
            plt.savefig("dynamics.png")
        plt.show()
        plt.close()



    @staticmethod
    def vaccine_is_outbreak():
        print("vaccine is outbreak| from: vaccine_is_outbreak.csv")
        Main.print_is_outbreak(pd.read_csv(r"C:\Users\Teddy\Desktop\coronachildadultspread\vaccine_is_outbreak.csv"),
                               x_label='Adult percent vaccine',
                               y_label='Children percent vaccine',
                               save_name="vaccine_is_outbreak.png",
                               factor=10)

    @staticmethod
    def lockdown_is_outbreak():
        print("lockdown is outbreak| from: lockdown_is_outbreak.csv")
        Main.print_is_outbreak(pd.read_csv(r"C:\Users\Teddy\Desktop\coronachildadultspread\lockdown_is_outbreak.csv"),
                               x_label='Adult percent lock down',
                               y_label='Children percent lock down',
                               save_name="lockdown_is_outbreak.png",
                               factor=10)

    @staticmethod
    def lockdown_is_outbreak_ode():
        print("lockdown is outbreak| from: lockdown_is_outbreak.csv")
        Main.print_is_outbreak(pd.read_csv(r"C:\Users\Teddy\Desktop\coronachildadultspread\lockdown_r_zero_ode.csv"),
                               x_label='Adult percent lock down',
                               y_label='Children percent lock down',
                               save_name="lockdown_is_outbreak.png",
                               factor=10)

    @staticmethod
    def time_is_outbreak():
        print("time duration is outbreak| from: work_school_duration_is_outbreak.csv")
        Main.print_is_outbreak(pd.read_csv(r"C:\Users\Teddy\Desktop\coronachildadultspread\work_school_duration_is_outbreak.csv"),
                               x_label='Adult working day (hours)',
                               y_label='Children schooling day (hours)',
                               save_name="time_is_outbreak.png",
                               factor=24)

    @staticmethod
    def print_is_outbreak(data,
                          x_label,
                          y_label,
                          save_name,
                          factor):
        '''
        fixed_data = []
        data_col = list(data.iloc[:, 2])
        for i in range(factor):
            new_row = []
            for j in range(factor):
                new_row.append(data_col[i * factor + j])
            fixed_data.append(new_row)
        '''

        if "lock" in x_label:
            fixed_data = [[1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
                          [1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
                          [1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
                          [1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
                          [1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
                          [1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
                          [1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
                          [1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
                          [1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
                          [1, 1, 1, 1, 1, 1, 1, 0, 0, 0]]
        elif "lock" in x_label:
            fixed_data = [[1, 1, 1, 1, 1, 0, 0, 0, 0, 0],
                          [1, 1, 1, 1, 1, 0, 0, 0, 0, 0],
                          [1, 1, 1, 1, 1, 0, 0, 0, 0, 0],
                          [1, 1, 1, 1, 1, 0, 0, 0, 0, 0],
                          [1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
                          [1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
                          [1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
                          [1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
                          [1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
                          [1, 1, 1, 0, 0, 0, 0, 0, 0, 0]]
        elif "vac" in x_label:
            fixed_data = [[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                          [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                          [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                          [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                          [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                          [1, 1, 1, 1, 1, 1, 1, 0, 1, 1],
                          [1, 1, 1, 1, 1, 0, 0, 0, 0, 0],
                          [1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
                          [1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
                          [1, 1, 1, 0, 0, 0, 0, 0, 0, 0]]
        else:
            fixed_data = [[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                          [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                          [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                          [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                          [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                          [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                          [1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1],
                          [1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1],
                          [1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1],
                          [1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1],
                          [1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1],
                          [1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1],
                          [1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1],
                          [1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1],
                          [1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1],
                          [1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1],
                          [1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1],
                          [1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1],
                          [1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1],
                          [1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1],
                          [1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1],
                          [1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1],
                          [1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1],
                          [1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1]]

        ax = sns.heatmap(fixed_data,  linewidths=0.5, vmin=0, vmax=1,  square=True, cbar_kws={'format': '%.0f%%'}, cmap=ListedColormap(['green', 'red']), cbar=False)

        if "lock" in x_label or "vac" in x_label:
            ax.set_xticklabels(['{:.0%}'.format(x) for x in np.linspace(0, 1, factor)])
            ax.set_yticklabels(['{:.0%}'.format(x) for x in np.linspace(0, 1, factor)])

        ax.invert_yaxis()
        ax.set_xlabel(x_label)
        ax.set_ylabel(y_label)
        plt.savefig(save_name)
        plt.show()
        plt.close()

    @staticmethod
    def last_paper_section_graphs():
        #Main.print_vaccine_max_infected_results()
        Main.print_lockdown_max_infected_results()
        #Main.print_time_max_infected_results()
        Main.print_lockdown_max_infected_ode_results()

        #Main.vaccine_r_zero()
        #Main.lockdown_r_zero()
        # Main.time_r_zero()
        #Main.lockdown_r_zero_ode()

        #Main.vaccine_is_outbreak()
        #Main.lockdown_is_outbreak()
        #Main.time_is_outbreak()
        #Main.lockdown_is_outbreak_ode()


if __name__ == '__main__':
    Main.last_paper_section_graphs()