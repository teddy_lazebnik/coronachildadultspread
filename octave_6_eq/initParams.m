### GLOBAL PARAMS ###
# values from the paper's table - normalize to [hour] units if units not in hours
global alpha;
global gama_c; 
global gama_a;
global beta_cc;
global beta_aa;
global beta_ac;
global beta_ca;
global Nc;
global Na;
global sc_initial;
global sa_initial;
global ic_initial;
global ia_initial;
global rc_initial;
global ra_initial;
global days;
global needGraph;


### END - GLOBAL PARAMS ###

function none = initParams()
  global alpha;
  global gama_c; 
  global gama_a;
  global beta_cc;
  global beta_aa;
  global beta_ac;
  global beta_ca;
  global Nc;
  global Na;
  global sc_initial;
  global sa_initial;
  global ic_initial;
  global ia_initial;
  global rc_initial;
  global ra_initial;
  global days;
  
  # manully picked
  days = 4;
  
  needGraph = 1;
  
  # set initial condition
  sc_initial = 2800;
  sa_initial = 7199;
  ic_initial = 0;
  ia_initial = 1;
  rc_initial = 0;
  ra_initial = 0;
  
  # model parms
  alpha = 8.7811 * 10 ** (-6);
  gama_c = 2 / 24; 
  gama_a = 14 / 24;
  beta_cc = 0.308;
  beta_aa = 0.308;
  beta_ac = 0;
  beta_ca = 0.266;
  
  # pop sums
  Nc = sc_initial + ic_initial + rc_initial;
  Na = sa_initial + ia_initial + ra_initial;
endfunction