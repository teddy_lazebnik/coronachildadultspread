import pandas
import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import datetime, timedelta
from sklearn.linear_model import LinearRegression


def generate_graph():
    # load data
    df = pandas.read_csv(r"C:\Users\Teddy\Desktop\coronachildadultspread\covid_israel_graph\WHO-COVID-19-global-data.csv")
    # pull out just israel
    df = df[df["Country"] == "Israel"]
    y = df["New_cases"]
    start_date = datetime.strptime(list(df["Date_reported"])[0], "%Y-%m-%d")
    y = list(y)
    x = [(start_date + timedelta(days=i)) for i in range(len(y))]

    # filter relevant
    new_y = []
    x_close = []
    y_close = []
    x_open = []
    y_open = []
    new_x = []
    start_date = datetime(year=2020, month=8, day=15)
    end_date = datetime(year=2020, month=9, day=28)
    open_date = datetime(year=2020, month=9, day=1)
    for i in range(len(x)):
        if x[i] > start_date and x[i] < end_date:
            new_x.append(x[i])
            new_y.append(y[i])
            if x[i] < open_date:
                x_close.append(x[i])
                y_close.append(y[i])
            elif x[i] > open_date:
                x_open.append(x[i])
                y_open.append(y[i])
    # find linear regression
    model_close = LinearRegression()
    model_open = LinearRegression()
    model_close.fit([[i] for i in range(len(x_close))], y_close)
    model_open.fit([[i] for i in range(len(x_open))], y_open)
    r2_close = math.pow(model_close.score([[i] for i in range(len(x_close))], y_close), 2)
    r2_open = math.pow(model_open.score([[i] for i in range(len(x_open))], y_open), 2)

    fig, ax = plt.subplots()
    ax.plot(new_x,
            new_y,
            '-k',
            label='New Cases Israel Daily')
    ax.plot([datetime(year=2020, month=9, day=1), datetime(year=2020, month=9, day=1)],
            [0, 8000],
            '-b',
            label='Open schools (1.9.2020)')
    # ax.plot([datetime(year=2020, month=6, day=15), datetime(year=2020, month=6, day=15)], [0, 550], '--r', label='$\gamma_a$ days later (15.6.2020)')
    ax.plot(x_close,
            [model_close.intercept_ + model_close.coef_ * i for i in range(len(x_close))],
            '-.g',
            label='new cases $\sim$ {:.2f}t + {:.2f}'.format(list(model_close.coef_)[0], model_close.intercept_))
    ax.plot(x_open,
            [model_open.intercept_ + model_open.coef_ * i for i in range(len(x_open))],
            '--.g',
            label='new cases $\sim$ {:.2f}t + {:.2f}'.format(list(model_open.coef_)[0],model_open.intercept_))
    ax.format_xdata = mdates.DateFormatter('%m-%d')
    ax.set_xlabel('Date')
    ax.set_ylabel('New COVID-19 cases')
    leg = ax.legend(loc='upper left')
    fig.autofmt_xdate()
    plt.gca().spines['right'].set_color('none')
    plt.gca().spines['top'].set_color('none')
    plt.savefig("intro2.png")
    plt.show()
    plt.close()


if __name__ == '__main__':
    generate_graph()
