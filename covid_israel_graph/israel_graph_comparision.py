import pandas
import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import datetime, timedelta
from sklearn.linear_model import LinearRegression


def generate_graph():
    # load data
    df = pandas.read_csv(r"C:\Users\Teddy\Desktop\coronachildadultspread\covid_israel_graph\WHO-COVID-19-global-data.csv")
    # pull out just israel
    df = df[df["Country"] == "Israel"]
    y_new = df["New_cases"]
    y_sum = df["Cumulative_cases"]
    start_date = datetime.strptime(list(df["Date_reported"])[0], "%Y-%m-%d")
    y_new = list(y_new)
    y_sum = list(y_sum)
    y = [0]
    y.extend([100 * (y_sum[index + 1] - y_sum[index]) / y_sum[index + 1] if y_sum[index + 1] != 0 else 0
              for index in range(len(y_sum) - 1)])
    x = [(start_date + timedelta(days=i)) for i in range(len(y))]

    y_model = [1.415, 1.534, 1.783, 1.881, 1.543, 1.701, 1.798, 1.355, 1.378, 1.501, 1.763, 2.137, 2.312, 2.098, 1.974, 1.503, 1.566, 1.782, 1.912, 2.341, 2.447, 2.514, 2.124, 2.348, 2.564, 2.780, 2.609, 2.178, 1.978, 2.101]
    y_model_npi = [1.415, 1.534, 1.783, 1.881, 1.543, 1.701, 1.798, 1.355, 1.378, 1.501, 1.763, 2.137, 2.312, 2.098, 1.974, 1.503, 1.566, 1.613, 1.678, 1.656, 1.584, 1.354, 1.415, 1.289, 1.333, 1.407, 1.431, 1.374, 1.308, 1.411]

    # filter relevant
    new_y = []
    x_close = []
    y_close = []
    y_model_close = []
    x_open = []
    y_open = []
    y_model_open = []
    new_x = []
    start_date = datetime(year=2020, month=8, day=15)
    end_date = datetime(year=2020, month=9, day=15)
    open_date = datetime(year=2020, month=9, day=1)

    hit_y_model = 0
    for i in range(len(x)):
        if x[i] > start_date and x[i] < end_date:
            new_x.append(x[i])
            new_y.append(y[i])
            if x[i] < open_date:
                x_close.append(x[i])
                y_close.append(y[i])
                y_model_close.append(y_model[hit_y_model])
            elif x[i] > open_date:
                x_open.append(x[i])
                y_open.append(y[i])
                y_model_open.append(y_model[hit_y_model])
            hit_y_model += 1
    model_close = LinearRegression()
    model_open = LinearRegression()
    model_hybrid_close = LinearRegression()
    model_hybrid_open = LinearRegression()
    model_close.fit([[i] for i in range(len(x_close))], y_close)
    model_open.fit([[i] for i in range(len(x_open))], y_open)
    model_hybrid_close.fit([[i] for i in range(len(x_close))], y_model_close)
    model_hybrid_open.fit([[i] for i in range(len(x_open))], y_model_open)

    fig, ax = plt.subplots()
    ax.plot(new_x,
            new_y,
            '-ok',
            label='Historical Data [4]')
    ax.plot(new_x,
            y_model_npi,
            '-^g',
            label='Hybrid Model - optimal NPI')
    ax.plot(new_x,
            y_model,
            '-*r',
            label='Hybrid Model')
    ax.plot([datetime(year=2020, month=9, day=1), datetime(year=2020, month=9, day=1)],
            [0, 3],
            '-b',
            label='Open schools (1.9.2020)')
    """
    ax.plot(x_close,
            [model_close.intercept_ + model_close.coef_ * i for i in range(len(x_close))],
            '-.k',
            label='R0 $\sim$ {:.2f}t + {:.2f}'.format(list(model_close.coef_)[0], model_close.intercept_))
    ax.plot(x_open,
            [model_open.intercept_ + model_open.coef_ * i for i in range(len(x_open))],
            '--.k',
            label='R0 $\sim$ {:.2f}t + {:.2f}'.format(list(model_open.coef_)[0],model_open.intercept_))
    ax.plot(x_close,
            [model_hybrid_close.intercept_ + model_hybrid_close.coef_ * i for i in range(len(x_close))],
            '-.g',
            label='R0 $\sim$ {:.2f}t + {:.2f}'.format(list(model_hybrid_close.coef_)[0], model_hybrid_close.intercept_))
    ax.plot(x_open,
            [model_hybrid_open.intercept_ + model_hybrid_open.coef_ * i for i in range(len(x_open))],
            '--.g',
            label='R0 $\sim$ {:.2f}t + {:.2f}'.format(list(model_hybrid_open.coef_)[0],model_hybrid_open.intercept_))
    """

    print("MSE = {:.3f}".format(sum([math.pow(new_y[index] - y_model[index], 2) for index in range(len(new_y))])/len(new_y)))

    ax.format_xdata = mdates.DateFormatter('%m-%d')
    ax.set_xlabel('Date')
    ax.set_ylabel('Basic reproduction numbers (R0)')
    leg = ax.legend(loc='bottom right')
    fig.autofmt_xdate()
    plt.gca().spines['right'].set_color('none')
    plt.gca().spines['top'].set_color('none')
    plt.savefig("compare_israel.png")
    plt.show()
    plt.close()


if __name__ == '__main__':
    generate_graph()
